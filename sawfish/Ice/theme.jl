; Ice/theme.jl

;; Ice Sawfish theme
;;
;; Design and Graphics by Arlo Rose
;; Implemented by Kenny Graunke and Josh Barrow
;; Based off of the "Eazel" Sawfish theme.
;;
;; � 2000 Eazel, Inc.

(require 'gtkrc)

(defgroup Ice-frame "Ice Settings")

(defcustom Ice:match-gtk-theme nil
           "Match current GTK theme"
           :group Ice-frame
           :type boolean
           :after-set after-setting-frame-option)

(let*
    ;; Update window title pixel length
    ((recolor-image
      (lambda (img color)
	(when (and (imagep img) (colorp color))
	  (let*
	      ((rgb (color-rgb color))
               ;; Perhaps it's just me, but I think the GTK-theme color matching
               ;; is always a little too dark, so I lighten it a bit. Also, I
               ;; refuse to recolor to match black (or very dark) GTK themes,
               ;; since then the *entire* frame is black, making it practically
               ;; unusable. So I darken it only to a certain point.
               (modify-color
                (lambda (i)
                  (max 85 (min 255 (+ i 15)))))
	       (red (nth 0 rgb))
	       (green (nth 1 rgb))
	       (blue (nth 2 rgb)))
	    ;; use the color modifiers
	    ;(set-image-modifier
	    ; img 'nil '(255 255 255))
	    (set-image-modifier
	     img 'red (list (modify-color (/ red 256)) 255 (modify-color (/ red 256))))
	    (set-image-modifier
	     img 'green (list (modify-color (/ green 256)) 255 (modify-color (/ green 256))))
	    (set-image-modifier
	     img 'blue (list (modify-color (/ blue 256)) 255 (modify-color (/ blue 256))))))))
     (title-width
      (lambda (w)
	(let
            ((w-width (car (window-dimensions w))))
	  (max 0 (text-width (window-name w)
                             (get-font          "-adobe-helvetica-bold-r-normal-*-*-120-*-*-p-*-iso8859-1"))))))
     
     ; convenience functions:
     (unfocus-image
      (lambda (img)
        ;(set-image-modifier img 'nil '(158 158 158))))
        (set-image-modifier img 'nil '(235 235 235))))
     
     (make-image-list
      (lambda (filename)
        (list (unfocus-image (make-image filename)) (make-image filename))))
     
     ;; ********* TITLE-BAR *********
     ;; 3x18
     (titlebar-left-edge-images (make-image-list "titlebar-left-edge.png"))
     
     ;; 20x18
     (titlebar-textbg-images (make-image-list "titlebar-textbg.png"))
     
     ;; 67x18
     (titlebar-main-ice-scale-images (make-image-list "titlebar-main-ice-scale.png"))
     
     ;; 13x18
     (titlebar-left-ice-images (make-image-list "titlebar-left-ice.png"))     
     ;; 14x18
     (iconify-images (list (unfocus-image (make-image "min.png"))
                           (make-image "min.png") nil
                           (make-image "p-min.png")))
     
     ;; 14x18
     (maximize-images (list (unfocus-image (make-image "max.png"))
                            (make-image "max.png") nil
                            (make-image "p-max.png")))
     
     ;; 13x18
     (close-images (list (unfocus-image (make-image "close.png"))
                         (make-image "close.png") nil
                         (make-image "p-close.png")))
     
     ;; 5x18
     (titlebar-right-edge-images (make-image-list "titlebar-right-edge.png"))
     
     ;; ********** TOP-BORDER **********
     
     ;; 20x5
     (top-left-corner-images (make-image-list "top-left-corner.png"))
     
     ;; 16x5
     (top-repeat-images (make-image-list "top-repeat.png"))
     
     ;; 26x6
     (top-right-corner-images (make-image-list "top-right-corner.png"))
     
     ;; *********** LEFT-BORDER ***********
     
     ;; 6x16
     (left-repeat-images (make-image-list "left-repeat.png"))
     
     ;; ********** RIGHT-BORDER ***********
     
     ;; 5x16
     (right-repeat-images (make-image-list "right-repeat.png"))
     
     ;; ********** BOTTOM-BORDER **********
     
     ;; 29x6
     (bottom-left-corner-images (make-image-list "bottom-left-corner.png"))
     
     ;; 16x6
     (bottom-repeat-images (make-image-list "bottom-repeat.png"))
     
     ;; 26x6
     (bottom-right-corner-images (make-image-list "bottom-right-corner.png"))
     
     (all-images (list titlebar-left-edge-images titlebar-textbg-images
                       titlebar-main-ice-scale-images titlebar-left-ice-images
                       iconify-images maximize-images close-images
                       titlebar-right-edge-images top-left-corner-images
                       top-repeat-images top-right-corner-images
                       left-repeat-images right-repeat-images
                       bottom-left-corner-images bottom-repeat-images
                       bottom-right-corner-images))
     
     (rebuild-frames
      (lambda ()
	(when (and gtkrc-background (colorp (car gtkrc-background)))
	  (mapc (lambda (images)
		  (mapc (lambda (img)
			  (recolor-image img (car gtkrc-background)))
			images))
		all-images))
	(mapc (lambda (w)
		(when (eq (window-get w 'current-frame-style)
			  'brushed-gtk)
		  (set-window-frame-style w 'brushed-gtk)))
	      (managed-windows))))
     
     (frame `(
	      ;; **TITLE-BAR**
              ;; top left corner
              ((background . ,titlebar-left-edge-images)
	       (left-edge . -5)
	       (top-edge . -23)
	       (class . top-left-corner))
              
              ;; close button
	      ((background . ,close-images)
	       (left-edge . -2)
	       (top-edge . -23)
	       (class . close-button))
              
	      ;; left text bumper
              ((background . ,titlebar-left-ice-images)
	       (top-edge . -23)
               (left-edge . 11)
               ;(right-edge . ,(lambda (w) (+ (title-width w) 71)))
               (class . title))
              
              ;; window title
              ((background . ,titlebar-textbg-images)
	       (foreground . "black")
               (text . ,window-name)
               (x-justify . center)
	       (y-justify . center)          
	       (font . "-adobe-helvetica-bold-r-normal-*-*-120-*-*-p-*-iso8859-1")
               (top-edge . -23)
               (left-edge . 24)
               (width . ,title-width)
               (class . title))
              
              ;; ice stretch
	      ((background . ,titlebar-main-ice-scale-images)
	       (top-edge . -23)
               (left-edge . ,(lambda (w) (+ (title-width w) 24)))
               (right-edge . 27)
	       (class . title))
              
	      ;; minimize button
              ((background . ,iconify-images)
               (right-edge . 13)
               (top-edge . -23)
               (class . iconify-button))
              
	      ;; maximize button
	      ((background . ,maximize-images)
	       (right-edge . -1)
	       (top-edge . -23)
	       (class . maximize-button))
	      
	      ;; top right corner
	      ((background . ,titlebar-right-edge-images)		
	       (right-edge . -6)
	       (top-edge . -23)
	       (class . top-right-corner))
	      
	      
              
              
	      
	      ;; **TOP-BORDER**
              ;; top left corner (top border half)
              ((background . ,top-left-corner-images)
               (left-edge . -5)
               (top-edge . -4)
               (class . top-left-corner))
              
              ;; top stretch
              ((background . ,top-repeat-images)
               (left-edge . 15)
               (right-edge . 20)
               (top-edge . -4)
               (class . top-border))
              
              ;; top right corner (top border half)
              ((background . ,top-right-corner-images)
               (right-edge . -6)
               (top-edge . -4)
               (class . top-right-corner))
              
              
	      
	      ;; **LEFT-BORDER**
	      ;; left top corner (left border half)
	      ((background . ,left-repeat-images)
	       (left-edge . -5)
	       (top-edge . 0)
	       (height . 20)
	       (class . top-left-corner))
	      
	      ;; left border stretch
	      ((background . ,left-repeat-images)
	       (left-edge . -5)
	       (top-edge . 20)
	       (bottom-edge . 20)
	       (class . left-border))
	      
	      ;; left bottom corner (left border half)
	      ((background . ,left-repeat-images)
	       (left-edge . -5)
	       (bottom-edge . 0)
	       (height . 20)
	       (class . bottom-left-corner))
              
              
              
	      
	      ;; **BOTTOM-BORDER**
	      ;; bottom left grow
	      ((background . ,bottom-left-corner-images)
	       (left-edge . -5)
	       (bottom-edge . -6)
	       (class . bottom-left-corner))
	      
	      ;; bottom stretch
	      ((background . ,bottom-repeat-images)
	       (left-edge . 15)
	       (right-edge . 20)
	       (bottom-edge . -6)
	       (class . bottom-border))
	      
	      ;; bottom right grow
	      ((background . ,bottom-right-corner-images)
	       (right-edge . -6)
	       (bottom-edge . -6)
	       (class . bottom-right-corner))
              
              
              
              
	      ;; **RIGHT-BORDER**
	      ;; top right corner (right border half)
	      ((background . ,right-repeat-images)
	       (right-edge . -6)
	       (top-edge . 0)
	       (height . 20)
	       (class . top-right-corner))
              
	      ;; right stretch
	      ((background . ,right-repeat-images)
	       (right-edge . -6)
	       (top-edge . 20)
	       (bottom-edge . 20)
	       (class . right-border))
	      
	      ;; bottom right corner (right border half)
	      ((background . ,right-repeat-images)
	       (right-edge . -6)
	       (bottom-edge . 0)
	       (height . 20)
	       (class . bottom-right-corner))))
     
     (shaped-frame `(
                     ;; **TITLE-BAR**
                     ;; top left corner
                     ((background . ,titlebar-left-edge-images)
                      (left-edge . -5)
                      (top-edge . -23)
                      (class . top-left-corner))
                     
                     ;; close button
                     ((background . ,close-images)
                      (left-edge . -2)
                      (top-edge . -23)
                      (class . close-button))
                     
	             ;; left text bumper
                     ((background . ,titlebar-left-ice-images)
                      (top-edge . -23)
                      (left-edge . 11)
                      ;(right-edge . ,(lambda (w) (+ (title-width w) 71)))
                      (class . title))
                     
                     ;; window title
                     ((background . ,titlebar-textbg-images)
                      (foreground . "black")
                      (text . ,window-name)
                      (x-justify . center)
                      (y-justify . center)          
                      (font . "-adobe-helvetica-bold-r-normal-*-*-120-*-*-p-*-iso8859-1")
                      (top-edge . -23)
                      (left-edge . 24)
                      (width . ,title-width)
                      (class . title))
                     
                     ;; ice stretch
                     ((background . ,titlebar-main-ice-scale-images)
                      (top-edge . -23)
                      (left-edge . ,(lambda (w) (+ (title-width w) 24)))
                      (right-edge . 27)
                      (class . title))
                     
	             ;; minimize button
                     ((background . ,iconify-images)
                      (right-edge . 13)
                      (top-edge . -23)
                      (class . iconify-button))
                     
	             ;; maximize button
                     ((background . ,maximize-images)
                      (right-edge . -1)
                      (top-edge . -23)
                      (class . maximize-button))
                     
 	             ;; top right corner
                     ((background . ,titlebar-right-edge-images)		
                      (right-edge . -6)
                      (top-edge . -23)
                      (class . top-right-corner))
                     
                     ;; bottom shadow, part I
                     ((background . "black")
                      (height . 1)
                      (top-edge . -4)
                      (left-edge . -5)
                      (right-edge . -6)
                      (class . title))
                     
                     ;; bottom shadow, part II
                     ((background . "black")
                      (height . 1)
                      (top-edge . -3)
                      (left-edge . -3)
                      (right-edge . -6)
                      (class . title)))))
  
  (add-frame-style 'Ice
		   (lambda (w type)
		     (case type
		       ((default) frame)
		       ((transient) frame)
		       ((shaped) shaped-frame)
		       ((shaped-transient) shaped-frame))))
  (call-after-property-changed
   'WM_NAME (lambda ()
              (rebuild-frames-with-style 'Ice)))
  (when Ice:match-gtk-theme (unless batch-mode (rebuild-frames))
    (gtkrc-call-after-changed rebuild-frames)))
